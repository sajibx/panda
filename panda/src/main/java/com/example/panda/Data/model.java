package com.example.panda.Data;

class Data_User{
    private String name;
    private Integer age;

    public Data_User(){
    }
    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }
    public Integer getAge(){
        return age;
    }
    public void setAge(Integer age){
        this.age = age;
    }
}