package com.example.panda.Data;

import com.sun.istack.Nullable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
final public class Users implements Serializable {
    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO
    )
    @Nullable
    private final Long id_user;
    @Nullable
    private String name;
    @Nullable
    private Long phone;
    @Nullable
    private String password;


    public Users(@Nullable Long id_user, @Nullable String name, @Nullable Long phone, @Nullable String password) {
        this.id_user = id_user;
        this.name = name;
        this.phone = phone;
        this.password = password;
    }

    public Users() {
        super();
        id_user = null;
    }


    @Nullable
    public final Long getId_user() {
        return this.id_user;
    }

    @Nullable
    public final String getName() {
        return this.name;
    }

    public final void setName(@Nullable String name) {
        this.name = name;
    }

    @Nullable
    public final Long getPhone() {
        return this.phone;
    }

    public final void setPhone(@Nullable Long phone) {
        this.phone = phone;
    }

    @Nullable
    public final String getPassword() {
        return this.password;
    }

    public final void setPassword(@Nullable String password) {
        this.password = password;
    }
}



