package com.example.panda;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Controller
@SpringBootApplication
public class FoodPandaApplication {

	@Autowired
	@RequestMapping("/")
	@ResponseBody
	String test()
	{
		return "test";
	}

	public static void main(String[] args) {
		SpringApplication.run(FoodPandaApplication.class, args);
	}

}

@Configuration
@EnableWebMvc
class WebConfig implements WebMvcConfigurer {
		public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**");
		}
};

//@Bean
//fun requestLoggingFilter(): CommonsRequestLoggingFilter{
//		val loggingFilter = CommonsRequestLoggingFilter()
//		loggingFilter.setIncludeClientInfo(true)
//		loggingFilter.setIncludeQueryString(true)
//		loggingFilter.setIncludePayload(true)
//		return loggingFilter
//		}

@EnableJpaRepositories
class Config {

}