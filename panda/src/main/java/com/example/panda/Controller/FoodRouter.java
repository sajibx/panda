package com.example.panda.Controller;


import com.example.panda.Data.Users;
import com.example.panda.Interface.UserInterface;
import com.sun.istack.NotNull;
import org.hibernate.annotations.common.util.impl.Log;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/home")
public class FoodRouter {

    @NotNull
    private final UserInterface userRepo;

    public FoodRouter(UserInterface userRepo) {
        this.userRepo = userRepo;
    }

    @PostMapping("/test")
    String sajib(@RequestBody Data_User users)
    {

        Users usr = new Users(Long.valueOf(users.getAge()),users.getName(), Long.valueOf(users.getAge()), users.getName());
        try{
            userRepo.save(usr);
        }catch (Exception e)
        {
            System.out.println("sajib "+e.toString());
            return e.toString();
        }
        return users.getName();
    }


}

class Data_User{
    private String name;
    private Integer age;

    public Data_User(){
    }
    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }
    public Integer getAge(){
        return age;
    }
    public void setAge(Integer age){
        this.age = age;
    }
}
