package com.example.panda.Interface;


import com.example.panda.Data.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserInterface extends JpaRepository<Users, Long> {

    @Query("select  p from Users p where p.name  = :name")
    Users getByName(@Param("name") String name);

}
